#!/usr/bin/env bash

TOMCAT_VERSION="8.5.43"


apt-get -y update
# apt-get -y upgrade

# ===================
# = JAVA            =
# ===================
apt-get -y --fix-missing install openjdk-8-jre-headless



# ===================
# = TOMCAT          =
# ===================

apt-get -y install xsltproc

TOMCAT_BASE_PATH=/usr/local

groupadd tomcat

adduser --system --shell /bin/bash --gecos 'Tomcat Java Servlet and JSP engine' --ingroup tomcat --disabled-password --home /home/tomcat tomcat

mkdir -p ~/tmp
cd ~/tmp
wget http://www.us.apache.org/dist/tomcat/tomcat-8/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz
tar xvzf ./apache-tomcat-$TOMCAT_VERSION.tar.gz
rm ./apache-tomcat-$TOMCAT_VERSION.tar.gz
mkdir -p $TOMCAT_BASE_PATH/tomcat8

source /vagrant/bootstrap/tomcat/tomcat.sh

rm -rf ~/tmp/apache-tomcat-$TOMCAT_VERSION
