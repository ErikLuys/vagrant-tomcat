#!/bin/bash

TOMCAT_NAME=tomcat

SHUTDOWN_PORT=8005

CONNECTOR_PORT=8080
REDIRECT_PORT=8443

AJP_CONNECTOR_PORT=8009
AJP_REDIRECT_PORT=8443

JPDA_PORT=8000

mkdir -p /shared/$TOMCAT_NAME

cp -r ~/tmp/apache-tomcat-$TOMCAT_VERSION $TOMCAT_BASE_PATH/tomcat8/$TOMCAT_NAME

rm -f $TOMCAT_BASE_PATH/$TOMCAT_NAME
ln -s $TOMCAT_BASE_PATH/tomcat8/$TOMCAT_NAME $TOMCAT_BASE_PATH/$TOMCAT_NAME

cp /vagrant/bootstrap/tomcat/tomcat-users.xml $TOMCAT_BASE_PATH/$TOMCAT_NAME/conf/
cp /vagrant/bootstrap/tomcat/server.xml $TOMCAT_BASE_PATH/$TOMCAT_NAME/conf/


xsltproc -o $TOMCAT_BASE_PATH/$TOMCAT_NAME/webapps/manager/META-INF/context_new.xml /vagrant/bootstrap/tomcat/context.xsl $TOMCAT_BASE_PATH/$TOMCAT_NAME/webapps/manager/META-INF/context.xml
mv $TOMCAT_BASE_PATH/$TOMCAT_NAME/webapps/manager/META-INF/context_new.xml $TOMCAT_BASE_PATH/$TOMCAT_NAME/webapps/manager/META-INF/context.xml

sed -i s/@@SHUTDOWN_PORT@@/$SHUTDOWN_PORT/g $TOMCAT_BASE_PATH/$TOMCAT_NAME/conf/server.xml
sed -i s/@@AJP_CONNECTOR_PORT@@/$AJP_CONNECTOR_PORT/g $TOMCAT_BASE_PATH/$TOMCAT_NAME/conf/server.xml
sed -i s/@@AJP_REDIRECT_PORT@@/$AJP_REDIRECT_PORT/g $TOMCAT_BASE_PATH/$TOMCAT_NAME/conf/server.xml
sed -i s/@@CONNECTOR_PORT@@/$CONNECTOR_PORT/g $TOMCAT_BASE_PATH/$TOMCAT_NAME/conf/server.xml
sed -i s/@@REDIRECT_PORT@@/$REDIRECT_PORT/g $TOMCAT_BASE_PATH/$TOMCAT_NAME/conf/server.xml


chown -R tomcat:tomcat $TOMCAT_BASE_PATH/tomcat8
find $TOMCAT_BASE_PATH/$TOMCAT_NAME/bin/ -name "*.sh" | sudo xargs -i chmod +x {}


cp /vagrant/bootstrap/tomcat/tomcat.service /etc/systemd/system/$TOMCAT_NAME.service

sed -i s/@@TOMCAT_NAME@@/$TOMCAT_NAME/g /etc/systemd/system/$TOMCAT_NAME.service
sed -i s=@@TOMCAT_BASE_PATH@@=$TOMCAT_BASE_PATH=g /etc/systemd/system/$TOMCAT_NAME.service
sed -i s/@@JPDA_PORT@@/$JPDA_PORT/g /etc/systemd/system/$TOMCAT_NAME.service


sudo systemctl daemon-reload

sudo systemctl start $TOMCAT_NAME
sudo systemctl enable $TOMCAT_NAME